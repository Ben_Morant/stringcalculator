package org.benmor;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

class StringCalculatorTest {

    private static Stream<Arguments> provideArgumentsForAdd() {
        return Stream.of(Arguments.of("", 0), Arguments.of("1", 1), Arguments.of("1,2", 3),
                         Arguments.of("1,2,3,4,5", 15), Arguments.of("1\n2,3", 6), Arguments.of("//;\n1,2;3", 6),
                         Arguments.of("5,10,1664", 15));
    }

    @ParameterizedTest(name = "Add() should return {1} when {0} is passed")
    @MethodSource("provideArgumentsForAdd")
    void should_return_expected_result(String input, int expected) {
        int output = StringCalculator.add(input);
        assertThat(output).isEqualTo(expected);
    }


    @Test
    void should_throw_NegativeNumberException_when_Input_has_negative_numbers() {
        // GIVEN
        String input = "-5,2,-10,9";
        // WHEN & THEN
        assertThatThrownBy(() -> StringCalculator.add(input)).isInstanceOf(NegativeNumberException.class)
                                                             .hasMessageContaining(
                                                                     "Les nombres négatifs ne sont pas autorisés : " +
                                                                             "-5, -10");
    }


}
