package org.benmor;

import java.util.Arrays;
import java.util.List;

public class StringCalculator {

    private static final int TOP_NUMBER = 1000;
    private static final String CUSTOM_DELIMITER_START = "//";

    private static final String FIXED_DELIMITER = "[,\n]";

    private static final String NEGATIVE_SIGN = "-";
    private static int result = 0;


    static int add(String input) {
        if (input.length() == 1) {
            return Integer.parseInt(input);
        }
        if (input.startsWith(CUSTOM_DELIMITER_START)) {
            input = handleInputWithCustomDelimiter(input);
        }
        String[] numbers = input.split(FIXED_DELIMITER);
        if (input.contains(NEGATIVE_SIGN)) {
            handleInputWithNegativeSigns(numbers);
        }
        return input.isEmpty() ? result :  Arrays.stream(numbers).map(Integer::parseInt).filter(nb -> nb <= TOP_NUMBER).mapToInt(Integer::intValue)
                                                                 .sum();

    }

    private static String handleInputWithCustomDelimiter(String input) {
        String customDelimiter = getCustomDelimiter(input);
        String workingInput = getWorkingInput(input);
        String[] splitted = workingInput.split(customDelimiter);
        return String.join(",", splitted);
    }

    private static void handleInputWithNegativeSigns(String[] numbers) {
        List<String> negativeNumbers = Arrays.stream(numbers).map(Integer::parseInt).filter(nb -> nb < 0).map(
                String::valueOf).toList();
        if (negativeNumbers.size() == 1) {
            throw new NegativeNumberException();
        }
        if (negativeNumbers.size() > 1) {
            throw new NegativeNumberException(negativeNumbers);
        }
    }

    private static String getWorkingInput(String input) {
        String firstNewLine = "\n";
        int firstNewLinePosition = input.indexOf(firstNewLine);
        return input.substring(firstNewLinePosition + 1);
    }

    private static String getCustomDelimiter(String input) {
        return input.substring(StringCalculator.CUSTOM_DELIMITER_START.length(),
                               StringCalculator.CUSTOM_DELIMITER_START.length() + 1);
    }
}




