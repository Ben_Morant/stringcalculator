package org.benmor;

import java.util.List;

public class NegativeNumberException extends IllegalArgumentException {

    public NegativeNumberException() {
        super("Les nombres négatifs ne sont pas autorisés");
    }

    public NegativeNumberException(List<String> numbers) {
        super("Les nombres négatifs ne sont pas autorisés : " + String.join(", ", numbers));
    }
}
